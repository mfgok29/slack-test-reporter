# Slack Test Reporter

# Reports

A report has many suites

# Suites

A suite has many results

# Results

# Example

Converts this JUnit XML Test summary into a formatted Slack message

```json
{
  "total_time": 32328.18632,
  "total_count": 1345,
  "success_count": 1247,
  "failed_count": 0,
  "skipped_count": 98,
  "error_count": 0,
  "test_suites": [
    {
      "name": "ee:instance",
      "total_time": 6787.161369000001,
      "total_count": 320,
      "success_count": 300,
      "failed_count": 0,
      "skipped_count": 20,
      "error_count": 0,
      "suite_error": null,
      "test_cases": [
        {
          "status": "success",
          "name": "Verify In merge trains new thread discussion does not drop MR",
          "classname": "qa.specs.features.ee.browser_ui.4_verify.new_discussion_not_dropping_merge_trains_mr_spec",
          "file": "./qa/specs/features/browser_ui/4_verify/new_discussion_not_dropping_merge_trains_mr_spec.rb",
          "execution_time": 173.831842,
          "system_output": null,
          "stack_trace": null,
          "recent_failures": null
        },
        {
          "status": "success",
          "name": "Manage standard when admin approval is required allows user login after approval",
          "classname": "qa.specs.features.browser_ui.1_manage.login.register_spec",
          "file": "./qa/specs/features/browser_ui/1_manage/login/register_spec.rb",
          "execution_time": 173.831842,
          "system_output": null,
          "stack_trace": null,
          "recent_failures": null
        }
      ]
    },
    {
      "name": "ee:relative_url",
      "total_time": 6787.161369000001,
      "total_count": 320,
      "success_count": 300,
      "failed_count": 0,
      "skipped_count": 20,
      "error_count": 0,
      "suite_error": null,
      "test_cases": [
        {
          "status": "success",
          "name": "Verify In merge trains new thread discussion does not drop MR",
          "classname": "qa.specs.features.ee.browser_ui.4_verify.new_discussion_not_dropping_merge_trains_mr_spec",
          "file": "./qa/specs/features/browser_ui/4_verify/new_discussion_not_dropping_merge_trains_mr_spec.rb",
          "execution_time": 173.831842,
          "system_output": null,
          "stack_trace": null,
          "recent_failures": null
        },
        {
          "status": "failed",
          "name": "Manage standard when admin approval is required allows user login after approval",
          "classname": "qa.specs.features.browser_ui.1_manage.login.register_spec",
          "file": "./qa/specs/features/browser_ui/1_manage/login/register_spec.rb",
          "execution_time": 173.831842,
          "system_output": null,
          "stack_trace": null,
          "recent_failures": null
        }
      ]
    }
  ]
}
```

![example](https://i.ibb.co/WK2Y9KR/Screen-Shot-2021-04-14-at-4-10-20-PM.png)
