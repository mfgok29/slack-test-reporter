# frozen_string_literal: true

module SlackTestReporter
  RSpec.describe Report do
    describe 'attributes' do
      it { should respond_to :time, :count, :success, :fail, :skipped, :error }
      it { should respond_to :time=, :count=, :success=, :fail=, :skipped=, :error= }
      it { should respond_to :suites, :suites= }
    end
  end
end
