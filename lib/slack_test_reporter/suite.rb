# frozen_string_literal: true

module SlackTestReporter
  class Suite
    attr_accessor :name,
                  :time,
                  :total_count,
                  :total_successes,
                  :total_failures,
                  :total_skipped,
                  :total_errors,
                  :error_message

    attr_accessor :results
  end
end
