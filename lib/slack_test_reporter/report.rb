# frozen_string_literal: true

module SlackTestReporter
  # Representation of a Test Report
  # A Report has many Suites
  class Report
    attr_accessor :time, :count, :success, :fail, :skipped, :error

    attr_accessor :suites
  end
end
