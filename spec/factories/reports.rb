# frozen_string_literal: true

FactoryBot.define do
  factory :report do
    time { 15.00 }
    count { 100 }
    success { 25 }
    fail { 25 }
    skipped { 25 }
    error { 25 }
  end
end
